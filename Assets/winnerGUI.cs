﻿using UnityEngine;
using System.Collections;

public class winnerGUI : MonoBehaviour {

	public GUIStyle  winnerFont;
	public Texture2D crown;
	
	public static float coolTime = 10f;
	

	
	int figureLength=90;
	int figureHeight=90;
	
	int timeBarLength=450;
	int timeBarHeight=20;
	
	int highLightLength=100;
	int highLightHeight=100;

	Vector2 figurePosition = new Vector2 (Screen.width/2-430, Screen.height / 6-30);


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI()
	{
		if(ProcessControl.ifGameEnds())
			{
			
		if (ProcessControl.winner == 0) {
			GUI.depth=1;
			GUI.Label (new Rect (figurePosition.x-20, figurePosition.y - 40, figureLength, figureHeight), crown);
			GUI.Label (new Rect (figurePosition.x-20, figurePosition.y + 20, figureLength, figureHeight), "WINNER",winnerFont);
			
		}
		
		if (ProcessControl.winner == 1) {
			GUI.depth=1;
			GUI.Label (new Rect (Screen.width - figurePosition.x - figureLength + 35-20, figurePosition.y -40, figureLength, figureHeight), crown);
			GUI.Label (new Rect (Screen.width - figurePosition.x - figureLength + 35-20, figurePosition.y + 20, figureLength, figureHeight), "WINNER",winnerFont);
			
			
		}
		
		if (ProcessControl.winner == 2) {
			GUI.depth=1;
			GUI.Label (new Rect (figurePosition.x-20, Screen.height - figurePosition.y - figureHeight - 40, figureLength, figureHeight), crown);
			GUI.Label (new Rect (figurePosition.x-20, Screen.height - figurePosition.y - figureHeight +20, figureLength, figureHeight), "WINNER",winnerFont);
			
		}
		
		if (ProcessControl.winner == 3) {
			GUI.depth=1;
			GUI.Label (new Rect (Screen.width - figurePosition.x - figureLength + 35-20, Screen.height - figurePosition.y - figureHeight - 40, figureLength, figureHeight), crown);
			GUI.Label (new Rect (Screen.width - figurePosition.x - figureLength + 35-20, Screen.height - figurePosition.y - figureHeight + 20, figureLength, figureHeight), "WINNER",winnerFont);
			
		}
		}
	}
}
